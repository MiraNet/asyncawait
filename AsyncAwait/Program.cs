﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class Program
    {
        static void Main(string[] args)
        {
            //TaskFirst();
            TaskTwice();
            Console.ReadKey();
        }

        static void TaskFirst()
        {
            Task[] tasks = new Task[2]
{
                new Task(() => Console.WriteLine("Первый")),
                new Task(() => Console.WriteLine("Второй"))
};

            for (int i = tasks.Length - 1; i >= 0; i--)
            {
                tasks[i].Start();
            }
        }

        static void TaskTwice()
        {
            var tasks = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Запускаем задачи");
                var tasks2 = Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Задача запущена.");
                    Thread.Sleep(1500);
                    Console.WriteLine("Конец.");
                }, TaskCreationOptions.AttachedToParent);
            });
            tasks.Wait();
            Console.WriteLine("Success.");
        }
    }
}
